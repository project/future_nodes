Future Nodes
============

Provide access control to nodes based on publication dates. Nodes from the future cannot by accessed by normal visitors.
