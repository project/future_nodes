<?php

/**
 * @file future_nodes.module
 * When enabled, nodes with published (created) dates in the future will not be
 * accessible by users without the "bypass" permission.
 */

/**
 * Implements hook_permission().
 */
function future_nodes_permission() {
  return array(
    'bypass future nodes access' =>  array(
      'title' => t('Bypass Future Nodes access control'),
      'description' => t('Users with this permission will not have node access limits by publication date.'),
    ),
  );
}


/**
 * Helper function to decide if this account can bypass future nodes access.
 *
 * @param $account Object   Optional account. If not provided, uses current user.
 * @return bool             TRUE if you can bypass, FALSE if you cannot.
 */
function _future_nodes_account_bypass($account = NULL) {
  if (!isset($account)) {
    $account = $GLOBALS['user'];
  }
  // List of permissions which let you bypass future node access.
  $permit_permissions = array(
    // General node access bypass.
    'bypass node access',
    // Specific node access bypass
    'bypass future nodes access',
  );

  foreach ($permit_permissions as $permission) {
    // If any of these permissions are granted, return TRUE.
    if (user_access($permission, $account)) {
      return TRUE;
    }
  }

  // Otherwise return FALSE.
  return FALSE;
}


/**
 * Implements hook_node_access().
 */
function future_nodes_node_access($node, $op, $account) {
  // If this is a string, then we have no timestamp to compare to. Use ignore so
  // other node access modules can have their say.
  if (is_string($node)) {
    return NODE_ACCESS_IGNORE;
  }

  // Lets not touch the access rules for unpublished nodes.
  if ($node->status == 0) {
    return NODE_ACCESS_IGNORE;
  }

  if (_future_nodes_account_bypass($account, $node)) {
    return NODE_ACCESS_ALLOW;
  }

  // If the node was created before or on the request time, allow access. Otherwise allow.
  return ($node->created > REQUEST_TIME) ? NODE_ACCESS_DENY : NODE_ACCESS_IGNORE;
}


/**
 * Implements hook_query_TAG_alter().
 */
function future_nodes_query_node_access_alter(QueryAlterableInterface $query) {
  // Read meta-data from query, if provided.
  if (!$account = $query->getMetaData('account')) {
    $account = $GLOBALS['user'];
  }

  // Check if the user running the query can bypass access.
  if (_future_nodes_account_bypass($account)) {
    return;
  }

  // Add a condition to the query to ensure listed nodes are created on or before
  // the request time.
  $query->condition("n.created", REQUEST_TIME, '<=');
}